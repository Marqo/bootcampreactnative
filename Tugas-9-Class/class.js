//No 1
console.log('No 1')
console.log(' ')
console.log('Release 0')
class Animal {
    constructor(name) {
      this._name = name;
      this._legs = 4;
      this._cold_blooded = false;
    }
    get name() {
      return this._name;
    }
    get legs() {
        return this._legs;
    }
    set legs(amount) {
      return this._legs = amount;
    }
    get cold_blooded() {
        return this._cold_blooded
    }
  }
  
  sheep = new Animal("shaun");

  console.log(sheep._name);
  console.log(sheep._legs);
  console.log(sheep._cold_blooded);
console.log(' ')

console.log('Release 1')
class Ape extends Animal {
    constructor(name, amount) {
      super(name);
      this.legs = amount;
    }
    yell() {
      console.log("Auooo");
    }
  }
class Frog extends Animal {
    constructor(name){
        super(name);
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape ("kera sakti", 2)
sungokong.yell()
console.log(sungokong._name)
console.log(sungokong._legs)
console.log(sungokong._cold_blooded)

var kodok = new Frog ("buduk")
kodok.jump()
console.log(kodok._name)
console.log(kodok._legs)
console.log(kodok._cold_blooded)
console.log(' ')

// No 2
console.log('No 2')
class Clock {
    constructor ({template}) {
        this.template = template
    }
    render () {
        let date = new Date ()

        let hours = date.getHours()
        if (hours < 10) hours = '0' + hours

        let mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins

        let secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)

        console.log(output)
    }
    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.render()
        this.timer = setInterval(() => this.render(), 1000)            
    }
}
  
var clock = new Clock({template: 'h:m:s'});
clock.start();



