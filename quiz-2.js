console.log('starting!');

setTimeout(() => {
    console.log("timer completed!")
}, 10)
console.log('finished!')

console.log(' ');

const myPromise = new Promise ((resolve, reject) => {
    setTimeout(() => {
        resolve('Timer completed!')
    }, 1000);
})
.then((text) => { throw new Error ('failed!') } )
.catch(err => console.log(err))
.then(() => console.log("does that execute?"));

async function wait () {
    try {
        const result = await doSomething()
        console.log(result);
    } catch (error) {
        console.log('error!');
    }
}