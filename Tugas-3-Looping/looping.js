//Soal No 1
console.log('Soal No 1')
console.log('LOOPING PERTAMA')
var number = 2
while ( number <= 20 ) {

    console.log (number + '- I Love Coding')
    number+= 2
}

console.log('LOOPING KEDUA')
var number = 20
while ( number > 1 ) {
    console.log (number + '- I will become a mobile developer')
    number-= 2
}

//Soal No 3
console.log('Soal No 3')
var row = 4;

for (var i = 0; i < row; i++) {
	var p = '';

	for (var j = 0; j <=7; j++) {
		p += '#';
    }
	console.log(p);
}


//Soal No 4
console.log('Soal No 4')

var p = '';
for(var i=0; i<7; i++) {
    for(var j=0; j<=i; j++) {
        p += '#'
    }
    p += '\n';
}
console.log(p);

//Soal No 5
console.log('Soal No 5')
var s = '';
var first = ' ';
var second = '#';

for(var i=0; i<=8; i++) {
    s = '';
    for(var j=0; j<=7; j++) {
        if(j %2 !== 0 && i % 2 !== 0) {
            s += first;
        } else if(j %2 !== 0 && i %2 === 0) {
            s += second;
        } else if(j %2 === 0 && i %2 !== 0) {
            s += second;
        } else if(j %2 === 0 && i %2 === 0) {
            s += first;
    }
}
    console.log(s)
}
