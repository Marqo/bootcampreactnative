import React, { Component } from 'react';
import { View, StyleSheet,Image,TouchableOpacity,Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon1 from 'react-native-vector-icons/AntDesign';



export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        
        <Text style={{fontSize:35, fontWeight: 'bold',color:'#05838B'}}>Tentang Saya</Text>
        <Icon name='account-circle' size={200} color={'grey'} />
        <Text style={{fontSize:25,color:'#05838B',fontWeight: 'bold'}}>Marqo Udin</Text>
        <Text style={{fontSize:15,paddingBottom:10,color:'#3EC6FF'}}>React Native Developer</Text>
    
        <View style={styles.porto}>
            <Text style={{fontSize:15,padding:5}}>Portofolio</Text>
            <View style={{height:0.5,backgroundColor:'black'}}/>
             <View style={{justifyContent:'space-around',paddingTop:10,alignItems:'center',flexDirection:'row'}}>
             <TouchableOpacity style={{flexDirection:'column',alignItems:'center'}}>
             <Icon1 name='gitlab' size={45} color={'#05838B'} />
             <Text style={{fontSize:15}}>@MarUd</Text>
            </TouchableOpacity>
             <TouchableOpacity style={{flexDirection:'column',alignItems:'center'}}>
             <Icon1 name='github' size={45} color={'#05838B'} />
            <Text style={{fontSize:15}}>@MarUd</Text>
            </TouchableOpacity>
            </View>
         </View>
        <View style={styles.kontak}>
            <Text style={{fontSize:15,padding:5}}>Hubungi Saya</Text>
            <View style={{height:0.5,backgroundColor:'black'}}/>
            <View style={{justifyContent:'space-around',paddingTop:10,alignItems:'center'}}>
                <TouchableOpacity style={{flexDirection:'row'}}>
                <Icon name='facebook' size={45} color={'#05838B'} />
                <Text style={{fontSize:15,padding:12}}>Marqo Udin</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection:'row'}}>
                <Icon1 name='instagram' size={45} color={'#05838B'} />
                <Text style={{fontSize:15,padding:12}}>@Marqo</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection:'row'}}>
                <Icon1 name='twitter' size={45} color={'#05838B'} />
                <Text style={{fontSize:15,padding:12}}>@MarUd</Text>
                </TouchableOpacity>
            </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'column',
    justifyContent:'space-around',
    paddingTop:50,
    alignItems:'center'
  },
  porto:{
    width: 370,
    height: 130,
    backgroundColor: '#EFEFEF',
    borderWidth: 1,
    borderColor: '#EFEFEF',
    borderRadius: 20,
    margin:10
  },
  kontak:{
    width: 370,
    height: 210,
    backgroundColor: '#EFEFEF',
    borderWidth: 1,
    borderColor: '#EFEFEF',
    borderRadius: 20,
  }
})