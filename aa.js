async function wait () {
    try {
        const result = await doSomething()
        console.log(result);
    } catch (error) {
        console.log('error!');
    }
}

function wait(){ 
    doSomething() 
    .then(result =>{
        console.log(result)}); 
    .catch(error)=>{
        console.log('error')
    }) 
}