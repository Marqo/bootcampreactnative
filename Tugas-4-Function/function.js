//Soal No 1
console.log('Soal No 1')
function teriak () {
    return 'Halo Sanbers!';
}
console.log(teriak())
console.log(' ')

//Soal No 2
console.log('Soal No 2')
function kalikan (x, y) {
    return x*y;
}
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)
console.log(' ')

//Soal No 3
console.log('Soal No 3')
function introduce (name, age, address, hobby) {
    var kalimat = "Nama saya " + name + ", " + "umur saya " + age + ", " + "alamat saya di "+ address + ", " + "dan saya punya hobby yaitu" + " " + hobby 
    return kalimat;
}
var name = "Mohammad Qomarudin"
var age = 23
var address = "Sudikampiran, Sliyeg, Indramayu"
var hobby = "Bulutangkis"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)